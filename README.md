# Major's containers

Inspired by [jessfraz/dockerfiles](), I decided to put some of my commonly
used container builds up on GitLab! However, I build and run all of my
containers with [podman] on Fedora. Some of the container builds may look a
little different since I'm running them in podman without daemons or root
access.

[jessfraz/dockerfiles]: https://github.com/jessfraz/dockerfiles
[podman]: https://podman.io

## Containers

Here are the containers in this repository:

### CubicSDR

[CubicSDR] is an open source application for demodulating radio waves from
software defined radio (SDR) devices, such as the [RTL-SDR].

[CubicSDR]: https://cubicsdr.com
[RTL-SDR]: https://www.rtl-sdr.com/
