#!/bin/bash
set -eux -o pipefail

pushd /usr/src
    git clone --depth 1 https://github.com/pothosware/SoapyRTLSDR.git
    pushd SoapyRTLSDR
        mkdir -v build
        pushd build
            cmake ../
            make -j$(nproc)
            make install
            ldconfig
        popd
    popd
popd
SoapySDRUtil --info
rm -rf /usr/src/SoapyRTLSDR
