#!/bin/bash
set -eux -o pipefail

# From the recommended packages found at:
# https://wiki.codelite.org/pmwiki.php/Main/WxWidgets31Binaries#toc3
REPO=http://repos.codelite.org/wx3.1.2/wx3.1-packages/fedora/29
dnf -y install --disablerepo=fedora-modular --disablerepo=updates-modular \
    ${REPO}/wxBase31-3.1.2-1.fc29.x86_64.rpm \
    ${REPO}/wxGTK31-3.1.2-1.fc29.x86_64.rpm \
    ${REPO}/wxGTK31-devel-3.1.2-1.fc29.x86_64.rpm \
    ${REPO}/wxGTK31-gl-3.1.2-1.fc29.x86_64.rpm \
    ${REPO}/wxGTK31-media-3.1.2-1.fc29.x86_64.rpm
dnf clean all
