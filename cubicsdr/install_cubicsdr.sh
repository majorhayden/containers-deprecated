#!/bin/bash
set -eux -o pipefail

pushd /usr/src
    git clone --depth 1 https://github.com/cjcliffe/CubicSDR.git
    pushd CubicSDR
        mkdir -v build
        pushd build
            cmake ../ -DwxWidgets_CONFIG_EXECUTABLE=/usr/bin/wx-config
            make -j$(nproc)
            make install
            ls -al x64
        popd
    popd
popd
rm -rf /usr/src/CubicSDR
