#!/bin/bash
set -eux -o pipefail

pushd /usr/src
    git clone --depth 1 https://github.com/jgaeddert/liquid-dsp
    pushd liquid-dsp
        ./bootstrap.sh
        CFLAGS="-march=native -O3" ./configure --enable-fftoverride
        make -j$(nproc)
        make install
        ldconfig
    popd
popd
rm -rf /usr/src/liquid-dsp
