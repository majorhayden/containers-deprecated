#!/bin/bash
set -eux -o pipefail

pushd /usr/src
    git clone --depth 1 git://git.osmocom.org/rtl-sdr.git
    pushd rtl-sdr
        mkdir -v build
        pushd build
            cmake ../
            make -j$(nproc)
            make install
            ldconfig
        popd
    popd
popd
rm -rf /usr/src/rtl-sdr
